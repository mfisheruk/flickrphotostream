/*jshint esversion: 6 */

const express = require('express');
const path = require('path');
const app = express();
const request = require('request');
const http = require('http').Server(app)
const https = require('https')
const port = process.env.PORT || 5001;

app.listen(port, () => console.log(`Listening on port ${port}`));

app.use(express.static(path.join(__dirname, 'client/build')));

if(process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, 'client/build')));
  //
  app.get('*', (req, res) => {
    res.sendfile(path.join(__dirname = 'client/build/index.html'));
  })
}

app.get('/express_backend', (req, res) => {
  res.send({ express: 'YOUR EXPRESS BACKEND IS CONNECTED TO REACT' });
});

app.get('/flickrapi', function (req,res) {  

  //setTimeout(function(){ 

    request('https://api.flickr.com/services/feeds/photos_public.gne?format=json&tagmode=all&tags=safe', {json : true}, (err, response, body) =>{
      if(err){
       next(err)
        //throw new Error("404")
        //return console.log("error: " + err);
      }

      if(response.statusCode === 200){
        var removeStart = response.body.split('jsonFlickrFeed(');
        var cleanString = removeStart[1].substring(0, removeStart[1].length -1);
        return res.send(cleanString);
      }
      else{
        return null;
      }
    });   
    
  //},2000)
});




app.get('/flickrtagsearch/:searchterm', function (req,res) {  
  var searchterm = req.params.searchterm;
  console.log(searchterm);

  request(`https://api.flickr.com/services/feeds/photos_public.gne?format=json&tagmode=all&tags=safe${searchterm !== '' ? ','+searchterm : ''}`, {json : true}, (err, response, body) =>{
      if(err){
        return console.log(err);
      }

      if(response.statusCode === 200){
        var removeStart = response.body.split('jsonFlickrFeed(');
        var cleanString = removeStart[1].substring(0, removeStart[1].length -1);
        return res.send(cleanString);
      }
      else{
        return null;
      }
    });     
});

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname = 'client/build/index.html'));
});

app.get('/*', function (req, res) {
	res.sendFile(path.join(__dirname, 'client/build/index.html'), function(err){
    if(err){
      res.status(500).send(err)
    }
	});
});