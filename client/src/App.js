/*jshint esversion: 6 */

import React, { Component } from 'react';
import ImageComponent from './components/ImageComponent';
import './css/core.css';
import {debounce} from 'lodash';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      flickrData : [],
      isLoaded : true,
      data: null,
      searchTerm: '',
      msg: <div className="sitemsg"><p>Loading Images</p></div>
    }
    this.handleChange = this.handleChange.bind(this)
    this.searchRequest = this.searchRequest.bind(this)
    this.getMoreImages = this.getMoreImages.bind(this)       
  } 

  componentDidMount(){        
    this.getFlickrData(); 
    
    window.onscroll = () => {
      if (
        window.innerHeight + document.documentElement.scrollTop
        === document.documentElement.offsetHeight
      ) {
        this.getMoreImages();
      }
    };
  } 

  getFlickrData = async(req, res) => {
    this.setState({msg : <div className="sitemsg"><p>Loading Images</p></div>})
    try{
      await fetch('/flickrapi')      
        .then(res => res.json())
        .then(resitems => resitems.items)              
       .then(flickrState => flickrState.length > 0 ? 
        this.setState({flickrData: flickrState, msg : ""}): 
        this.setState({flickrData: flickrState, msg : <div className="sitemsg"><p>Sorry no images found for your query</p></div>})
        )  
    }
    catch{
      this.setState({msg: <div className="sitemsg"><p>Sorry no images can be found at this time, please try again later</p></div>})
    }    
  }

  getMoreImages = async(req,res) =>{
    var apiurl = '/flickrapi';

    if(this.state.searchTerm !== ""){
      apiurl = '/flickrtagsearch/' + this.state.searchTerm;
    }

    try{
      await fetch(apiurl)      
        .then(res => res.json())
        .then(resitems => resitems.items) 
        .then(more => [...this.state.flickrData, ...more])       
        .then(flickrState => this.setState({flickrData: flickrState}))  
    }
    catch{
      this.setState({msg: <div className="sitemsg"><p>Sorry no images can be found at this time, please try again later</p></div>})
    }
  }  

  handleChange(event){    
    this.setState({searchTerm: event.target.value}, function(){console.log("state : " + this.state.searchTerm)});    
    this.filterPhotos();
  }

  filterPhotos = debounce(photos => {
    this.searchRequest();
  }, 1000);

  searchRequest = async(req,res) =>{ 
    this.setState({msg : <div className="sitemsg"><p>Loading Images</p></div>}) 
      try{ 
        await fetch('/flickrtagsearch/' + this.state.searchTerm)        
          .then(res => res.json())
          .then(resitems => resitems.items)                 
          .then(flickrState => flickrState.length > 0 ? this.setState({flickrData: flickrState, msg : ""}) : this.setState({flickrData: flickrState, msg : <div className="sitemsg"><p>Sorry no images found for your query</p></div>}))         
      }
      catch{
        this.getFlickrData();
      }    
  }

  render() {       
    return (
      <div className="App">
        <header className="">
          <div className="headerarea">
            <h2>Flickr Photo Stream</h2>
            <div className="searchform">
              <input type="text" value={this.state.searchRequest} placeholder="Search tags" onChange={this.handleChange} ></input>
              <button type="submit" value={this.state.searchRequest} onClick={this.handleChange}>Search</button>
            </div>
          </div>
        </header>
        <div>
          <div className="maincontent"> 
                  <ImageComponent flickrData={this.state.flickrData} msg={this.state.msg} handleChange={this.handleChange} searchRequest={this.searchRequest} getMoreImages={this.getMoreImages} /> 
          </div>          
        </div>            
      </div>    
    );             
  }
}

export default App;