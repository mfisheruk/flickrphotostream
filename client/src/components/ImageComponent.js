/*jshint esversion: 6 */

import React, {Component} from 'react';

class ImageComponent extends Component {
    constructor(props){
      super(props);
      this.state = {
        
      }      
    }  
  
    render(){
        function createMarkup(markup) {
            return {__html: markup};
          }
          
          return(
              <div className="contentbody">
                {this.props.msg}
                <div className="imagecontainer">
                

                {this.props.flickrData.length > 0 && this.props.flickrData.map((item, index) =>{
                return(
                    <div className="imagecontent" key={index}>
                        <div className="contentarea">                        
                            <div className="contentinfo">                        
                                <a href={item.link}><img src={item.media.m} alt={item.description} /></a>                                           
                                <p className="title"><span title={item.title}>{item.title}</span> by <span><a href={`https://www.flickr.com/people/${item.author_id}`}>{item.author}</a></span></p>
                            
                                <p className="description">Description : 
                                    <span dangerouslySetInnerHTML={createMarkup(item.description)} />
                                </p>
                                <p className="tags">Tags : {item.tags.length > 0? item.tags : "Image not tagged"}</p>
                            </div>
                        </div>
                    </div>                    
                    )
                })}                
            </div>
        </div>
        )
    }
} 
    
export default ImageComponent;